﻿#region "copyright"

/*
    Copyright © 2016 - 2022 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#endregion "copyright"

using System;
using System.Collections.Generic;
using NINA.Equipment.Equipment.MyFlatDevice;
using NINA.Profile.Interfaces;
using NINA.Core.Utility;
using NINA.Core.Locale;
using NINA.Equipment.Utility;
using NINA.Equipment.Interfaces;
using NINA.Equipment.Interfaces.ViewModel;
using NINA.Equipment.Equipment;

namespace NINA.WPF.Base.ViewModel.Equipment.FlatDevice {

    public class FlatDeviceFactory : IDeviceFactory {
        private readonly IProfileService profileService;
        private readonly IDeviceDispatcher deviceDispatcher;

        public FlatDeviceFactory(IProfileService profileService, IDeviceDispatcher deviceDispatcher) {
            this.profileService = profileService;
            this.deviceDispatcher = deviceDispatcher;
        }

        public IList<IDevice> GetDevices() {
            var ascomInteraction = new ASCOMInteraction(deviceDispatcher, profileService);
            var devices = new List<IDevice>();
            devices.Add(new DummyDevice(Loc.Instance["LblFlatDeviceNoDevice"]));

            try {
                foreach (IFlatDevice flatDevice in ascomInteraction.GetCoverCalibrators()) {
                    devices.Add(flatDevice);
                }
            } catch (Exception ex) {
                Logger.Error(ex);
            }

            devices.AddRange(new List<IDevice>{
                new AllProSpikeAFlat(profileService),
                new AlnitakFlipFlatSimulator(profileService),
                new AlnitakFlatDevice(profileService),
                new ArteskyFlatBox(profileService),
                new PegasusAstroFlatMaster(profileService)
            });
            return devices;
        }
    }
}